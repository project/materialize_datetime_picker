/**
 * @file
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.materialize_datetime_picker = {
    attach: function (context, settings) {

      // Setting the current language for the calendar.
      var language = drupalSettings.path.currentLanguage;

      $(context).find('input[data-materialize-date-time]').once('datePicker').each(function () {
        var input = $(this);

        // Get widget Type.
        var widgetType = input.data('materializeDateTime');

        // Get hour format - 12 or 24.
        var hourFormat = input.data('hourFormat');
        var timeFormat = (hourFormat === '12h') ? 'YYYY-MM-DD hh:mm' : 'YYYY-MM-DD  HH:mm';

        // Get excluded dates.
        var excludeDates = '';
        if (typeof input.data('excludeDate') != 'undefined') {
          excludeDates = input.data('excludeDate').split(',');
        }

        // Get disabled days.
        var disabledDays = input.data('disableDays');

        // Get minute granularity.
        var allowedTimes = input.data('allowTimes');

        // Get week start.
        var weekStart = input.data('weekStart');

        var DatetimeOnly = [];
        var DateOnly = [];
        if(drupalSettings.materialize_data) {
          DatetimeOnly = drupalSettings.materialize_data.datetime_data;
          if(DatetimeOnly != null && jQuery.inArray(input.attr('id'), DatetimeOnly)) {
            for ( var i = 0, l = DatetimeOnly.length; i < l; i++ ) {
                $('#' + DatetimeOnly[ i ]).bootstrapMaterialDatePicker({
                  format: timeFormat,
                  disabledDays: disabledDays,
                  disabledDates: excludeDates,
                  minuteStep: allowedTimes,
                  lang: language,
                  weekStart: weekStart,
                  clearButton: true
                });
            }
          }
        }
        if(drupalSettings.materialize_data) {
          DateOnly = drupalSettings.materialize_data.date_data;
          if(DateOnly != null && jQuery.inArray(input.attr('id'), DateOnly)) {
            for ( var i = 0, l = DateOnly.length; i < l; i++ ) {
                $('#' + DateOnly[ i ]).bootstrapMaterialDatePicker({
                  format: 'YYYY-MM-DD',
                  disabledDays: disabledDays,
                  disabledDates: excludeDates,
                  minuteStep: allowedTimes,
                  lang: language,
                  time: false,
                  weekStart: weekStart,
                  clearButton: true
                });
            }
          }
        }
        // If field widget is Date Time.
        if (widgetType === 'datetime') {
          $('#' + input.attr('id')).bootstrapMaterialDatePicker({
            format: timeFormat,
            disabledDays: disabledDays,
            disabledDates: excludeDates,
            minuteStep: allowedTimes,
            lang: language,
            weekStart: weekStart,
            clearButton: true
          });
        }
        // If field widget is Date only.
        else {
          $('#' + input.attr('id')).bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            disabledDays: disabledDays,
            disabledDates: excludeDates,
            minuteStep: allowedTimes,
            lang: language,
            time: false,
            weekStart: weekStart,
            clearButton: true
          });
        }
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
