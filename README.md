CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Author/Maintainers

INTRODUCTION
-----------

 * Drupal 8 Bootstrap Materialize DateTime picker. Bootstrap Materialize
   DateTime Picker module provides a widget for Drupal Date fields

REQUIREMENTS
------------

 * None.

INSTALLATION
------------

 * To install, copy the Bootstrap Materialize DateTime Picker directory and
   all its contents to your modules directory.

 * To enable the module go to Administer > Modules, and enable "Bootstrap 
   Materialize DateTime Picker".

CONFIGURATION
-------------
 * Create a Date field in a content type.

 * Edit the Content type and navigate to "Manage form display" tab.

 * Under "Widget" select widget for Date/DateTime for "Bootstrap Materialize
   DateTime Picker".

 * From the settings icon you can define configurations for the "Bootstrap 
   Materialize DateTime Picker" widget.

 * Hours Format - Hours format for the DateTime calendar.

 * Minutes Granularity - Increments minutes in DateTime calendar when clicking
   on up/down arrows.

 * Disable specific days in week - Disable dates of specific days such as
   Monday, Sunday etc.

 * Define start week - Define specific day to start your calendar.

 * Disable specific dates from calendar - If you want to disable specific dates
   from the calendar.

 * Separate multiple dates with comma.

 * Format should be YYYY-MM-DD e.g. 2020-03-21.

AUTHOR/MAINTAINERS
------------------
Author:
 * Arun Kumar - https://www.drupal.org/u/arunkumarkit
